package com.firstam.amp.utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.parser.JSONParser;

import java.io.*;

public abstract class RestTest {
    private static String urlXray = "https://xray.cloud.xpand-it.com";

    public static void addTextExecution() throws Exception {
        FileReader reportJSON = new FileReader("target/cucumber.json");
        JSONParser jsonParser = new JSONParser();
        RestAssured.baseURI = urlXray;
        Response response = RestAssured.given()
                .contentType("application/json")
                .header("Authorization", "Bearer " + getToken())
                .body(jsonParser.parse(reportJSON))
                .post("/api/v1/import/execution/cucumber");

        response.getBody().prettyPrint();
    }

    private static String getToken() {
        RestAssured.baseURI = urlXray;
        Response response = RestAssured.given()
                .contentType("application/json")
                .body("{ \"client_id\": \"0910B57AC0F646CE921727C48CF2D488\", \"client_secret\": \"88678099e02517cbb64058dc8c8e83de1096c7c687eac9d68f74a8d130cc161f\" }")
                .post("/api/v1/authenticate");
        return response.getBody().asString().replace("\"", "");
    }
}