package com.firstam.amp.utils;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.JavascriptExecutor;

public class DriverFactory {
    private static boolean initialized = false;
//    public static String sConfiPropFilePath = "src/test/java/com/firstam/amp/utils/Configuration.properties";
    protected static WebDriver driver;
    private static WebDriver incognitoDriver;


    //BrowserStack
    private static final String BROWSERSTACK_USERNAME = "justynamarkowska1";
    private static final String BROWSERSTACK_AUTOMATE_KEY = "gBgfHvMSZFBAdzSLiWry";
    private static final String BROWSERSTACK_URL = "https://" + BROWSERSTACK_USERNAME + ":" + BROWSERSTACK_AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";


    public static WebDriver getDriver() {
        if (driver == null) {
            try {
                setUp();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        return driver;
    }
    public static WebDriver getIncognitoDriver() {
        if (incognitoDriver == null) {
            try {
                createIncognitoDriver();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        return incognitoDriver;
    }

    public static void setUp() {
        if (!initialized) {
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Drivers\\" + "chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
                options.addArguments("start-maximized");
                driver = new ChromeDriver(options);
                driver.manage().window().maximize();
            if (!(driver == null)) {
                driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
                ((JavascriptExecutor) driver).executeScript("window.focus();");
                driver.get("https://devamp.firstam.com/Resources/Home.aspx?ReturnUrl=%2F");
                initialized = true;
            }
        }
    }

    private static void createIncognitoDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        options.addArguments("start-maximized");
        options.addArguments("--incognito");
        incognitoDriver = new ChromeDriver(options);
    }

    //BrowserStack constructor
    public static void setUpBrowserStack() throws MalformedURLException {
        if (!initialized) {

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "74.0");
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("resolution", "1920x1080");
            caps.setCapability("name", "Bstack-[Java] Sample Test");
            caps.setCapability("browserstack.local", "true");

            driver = new RemoteWebDriver(new URL(BROWSERSTACK_URL), caps);
            driver.manage().window().maximize();
            if (!(driver == null)) {
                driver.manage().timeouts().implicitlyWait(1500, TimeUnit.MILLISECONDS);
                ((JavascriptExecutor) driver).executeScript("window.focus();");
                driver.get("https://devamp.firstam.com/Resources/Home.aspx?ReturnUrl=%2F");
                initialized = true;
            }
        }
    }

    //BrowserStack incognito constructor
    public static void createIncognitoBrowserStack() throws MalformedURLException {
        if (!initialized) {

            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "74.0");
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "10");
            caps.setCapability("resolution", "1920x1080");
            caps.setCapability("name", "Bstack-[Java] Sample Test");
            caps.setCapability("browserstack.local", "true");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--incognito");
            caps.setCapability(ChromeOptions.CAPABILITY, options);

            incognitoDriver = new RemoteWebDriver(new URL(BROWSERSTACK_URL), caps);
            incognitoDriver.manage().window().maximize();
        }
    }
}
