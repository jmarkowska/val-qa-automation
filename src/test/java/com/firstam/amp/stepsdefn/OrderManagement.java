package com.firstam.amp.stepsdefn;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import com.firstam.amp.utils.DriverFactory;

import java.io.FileNotFoundException;


public class OrderManagement {

    private WebDriver driver = DriverFactory.getDriver();
    private com.firstam.amp.pages.OrderManagement orderManagement = new com.firstam.amp.pages.OrderManagement(driver);
    private com.firstam.amp.pages.UserPanel userPanel = new com.firstam.amp.pages.UserPanel(driver);

    @When("^User types in Order Number \"([^\"]*)\"$")
    public void userTypesInOrderNumber(String orderNumber) {
        orderManagement.userTypesInOrderNumber(orderNumber);
    }

    @And("^User double clicks on Order$")
    public void userDoubleClicksOnOrder() {
        orderManagement.userDoubleClicksOnOrder();
    }

    @Then("^User sees Order Details$")
    public void userSeesOrderDetails() {
        Assert.assertTrue(orderManagement.userSeesOrderDetails());
    }

    @When("^User clicks on VM Assist button$")
    public void userClicksOnVMAssistButton() {
        orderManagement.userClicksOnVMAssistButton();
    }

    @Then("^User sees Confirm VM Assist window$")
    public void userSeesConfirmVMAssistWindow() {
        Assert.assertTrue(orderManagement.userSeesConfirmVMAssistWindow());
    }

    @And("^User clicks on Cancel option$")
    public void userClicksOnCancelOption() {
        orderManagement.userClicksOnCancelOption();
    }

    @And("^User sees Cancel button in confirm VM Assist window$")
    public void userCancelButtonInConfirmVMAssistWindow() {
        Assert.assertTrue(orderManagement.userSeesCancelButtonInConfirmVMAssistWindow());
    }

    @And("^User sees Confirm button in confirm VM Assist window$")
    public void userConfirmButtonInConfirmVMAssistWindow() {
        Assert.assertTrue(orderManagement.userSeesConfirmButtonInConfirmVMAssistWindow());
    }

    @When("^User clicks on Confirm option$")
    public void userClicksOnConfirmOption() {
        orderManagement.userClicksOnConfirmOption();
    }

    @And("^User types in \"([^\"]*)\" in Confirm VM Assist window$")
    public void userTypesInInConfirmVMAssistWindow(String text) {
        orderManagement.userTypesInInConfirmVMAssistWindow(text);
    }

    @And("^User clicks Save button$")
    public void userClicksSaveButton() {
        orderManagement.userClicksSaveButton();
    }

    @Given("^User clicks on Complete Task button$")
    public void userClicksOnCompleteTaskButton() {
        orderManagement.userClicksOnCompleteTaskButton();
    }

    @Then("^User sees VM Assist button$")
    public void userSeesVMAssistButton() {
        Assert.assertTrue(orderManagement.userSeesVMAssistButton());
    }

    @And("^User clicks on Back button$")
    public void userClicksOnBackButton() {
        orderManagement.userClicksOnBackButton();
    }

    @When("^User clicks on (\\d+) order on the list$")
    public void userClicksOnOrderOnTheList(int number) {
        orderManagement.clicksOnOrderOnTheList(number);
    }

    @When("^User clicks on Next button$")
    public void userClicksOnNextButton() {
        orderManagement.clickNextButton();
    }

    @And("^Complete Task button is displayed$")
    public void completeTaskButtonIsDisplayed() {
        Assert.assertTrue("Complete button is not displayed", orderManagement.isCompleteTaskDisplayed());
    }

    @And("^VM Assist button is not displayed$")
    public void vmAssistButtonIsNotDisplayed() {
        Assert.assertFalse("VM Assist button is displayed", orderManagement.isVmAssistButtonDisplayed());
    }

    @Then("^VM Assist button is displayed$")
    public void vmAssistButtonIsDisplayed() {
        Assert.assertTrue(orderManagement.isVmAssistButtonDisplayed());
    }

    @Then("^VM Assist Task Completed message is displayed$")
    public void vmAssistTaskCompletedMessageIsDisplayed() {
        Assert.assertTrue("Message is not displayed", orderManagement.isPageMessageDisplayed());
        Assert.assertTrue("It's not a VM Assist Task Completed message", orderManagement.isVmAssistTaskCompletedMessageDisplayed());
    }

    @And("^Add (\\d+) orders to VM Assistance SQ$")
    public void addOrdersToVMAssistanceSQ(int number) {
        for(int i=1; i<=number; i++){
            userPanel.userClicksOnOrderManagement();
            userPanel.waitUntilLoaderDisappears();
            orderManagement.clicksOnOrderOnTheList(i+2);
            userPanel.waitUntilLoaderDisappears();
            Assert.assertTrue(orderManagement.userSeesOrderDetails());
            if(orderManagement.isVmAssistButtonDisplayed()) {
                orderManagement.userClicksOnVMAssistButton();
                Assert.assertTrue(orderManagement.userSeesConfirmVMAssistWindow());
                orderManagement.userTypesInInConfirmVMAssistWindow("test");
                orderManagement.userClicksOnConfirmOption();
                userPanel.waitUntilLoaderDisappears();
                userPanel.userClicksOnOrderManagement();
            }
        }
    }

    @And("^Second order ID is different from the first one$")
    public void secondOrderIdIsDifferentFromTheFirstOne() {
        Assert.assertFalse("Order IDs are the same", orderManagement.isSecondOrderIdTheSameAsTheFirstOne());
    }

    @And("^Third order ID is the same as the first one$")
    public void thirdOrderIDIsTheSameAsTheFirstOne() {
        Assert.assertTrue("Order IDs are different", orderManagement.isThirdOrderIdTheSameAsTheFirstOne());
    }

    @And("^Second order ID is the same as the first one$")
    public void secondOrderIDIsTheSameAsTheFirstOne() {
        Assert.assertTrue("Order IDs are different", orderManagement.isSecondOrderIdTheSameAsTheFirstOne());
    }

    @And("^Third order ID is different from previous$")
    public void thirdOrderIDIsDifferentFromPrevious() {
        Assert.assertFalse("Order IDs are the same", orderManagement.isThirdOrderIdTheSameAsTheFirstOne());
        Assert.assertFalse("Order IDs are the same", orderManagement.isThirdOrderIdTheSameAsTheSecondOne());
    }

    @When("^User clicks on Assign button$")
    public void userClicksOnAssignButton() {
        orderManagement.clickAssignButton();
    }

    @Then("^Order status is \"([^\"]*)\"$")
    public void checkOrderStatus(String status) {
        Assert.assertEquals("Order status is wrong", status, orderManagement.orderStatus());
    }

    @Then("^Message \"([^\"]*)\" is displayed$")
    public void messageIsDisplayed(String message) throws InterruptedException {
        orderManagement.setGlobalOrderID();
        Assert.assertTrue("Message is not displayed", orderManagement.isPageMessageDisplayed());
        if(!orderManagement.isPageMessageEmpty()) {
            Thread.sleep(1000);
            String similar = "";
            switch(message) {
                case "Vendor Management Assistance" :
                    similar = "VendorManagementAssistance";
                    break;
                case "Manually Assign Order – Denied Assignment" :
                    similar = "ManuallyAssignDenied";
                    break;
                case "Manually Assign Order – Unassigned":
                    similar = "ManuallyAssignUnassigned";
                    break;
                case "Manually Assign Order -  Unaccepted/Timeout":
                    similar = "ManuallyAssignTimeout";
                    break;
                case "Dened Order New Note":
                    similar = "DeniedOrderNewNote";
                    break;
                default:
                    break;
            }
            Assert.assertThat(orderManagement.getPageMessage(), CoreMatchers.either(CoreMatchers.is(message)).or(CoreMatchers.is(message.replace("–", "-"))).or(CoreMatchers.is(similar)));
        }
    }

    @And("^User goes to next page of order list$")
    public void userGoesToNextPageOfOrderList() {
        orderManagement.goToNextPage();
    }

    @When("^User completes all VM Assist Tasks$")
    public void userCompletesAllVMAssistTasks() throws InterruptedException {
        int counter = 1;
        userPanel.waitUntilLoaderDisappears();
        Thread.sleep(2000);
        while (!orderManagement.getPageMessage().equals("End of Queue.")) {
            Assert.assertEquals("Message is different", "Vendor Management Assistance", orderManagement.getPageMessage());
            Assert.assertTrue(orderManagement.isCompleteTaskDisplayed());
            orderManagement.userClicksOnCompleteTaskButton();
            userPanel.waitUntilLoaderDisappears();
            Assert.assertEquals("Message is different", "VM Assist Task Completed.", orderManagement.getPageMessage());
            System.out.println("LOG Task Completed: " + counter++);
            orderManagement.clickNextButton();
            userPanel.waitUntilLoaderDisappears();
            if (orderManagement.getPageMessage().equals("End of Queue.")) break;
        }
    }

    @And("^User clicks on Assign and Close button$")
    public void userClicksOnAssignAndCloseButton() {
        orderManagement.clickAssignAndCloseButton();
    }

    @Then("^Order list is displayed$")
    public void orderListIsDisplayed() {
        Assert.assertTrue("Order list is not displayed", orderManagement.isOrderListDisplayed());
    }

    @Then("^Vendor \"([^\"]*)\" is assigned to the order$")
    public void vendorIsAssignedToTheOrder(String vendor) {
        Assert.assertEquals("Order vendor is not assigned", vendor, orderManagement.getVendorAssignedToOrder());
    }
    @Then("^Vendor \"([^\"]*)\" is not assigned to the order$")
    public void vendorIsNotAssignedToTheOrder(String vendor) {
        Assert.assertNotEquals("Order vendor is assigned", vendor, orderManagement.getVendorAssignedToOrder());
    }

    @When("^User sets Follow Up date in the future$")
    public void userSetsFollowUpDateInTheFuture() throws InterruptedException {
        orderManagement.openFollowUpDateCalendar();
        userPanel.waitUntilLoaderDisappears();
        Assert.assertTrue("Follow Up date window is not displayed", orderManagement.isFollowUpDateWindowDisplayed());
        orderManagement.setFollowUpDateInTheFuture();
        Thread.sleep(2000);
        orderManagement.saveNewFollowUpDate();
        userPanel.waitUntilLoaderDisappears();
    }

    @And("^Order status is not \"([^\"]*)\"$")
    public void orderStatusIsNot(String status) {
        Assert.assertNotEquals("Order status is wrong", status, orderManagement.orderStatus());
    }

    @When("^User clicks on (\\d+) order with \"([^\"]*)\" status on the list$")
    public void userClicksOnOrderWithStatusOnTheList(int number, String status) throws FileNotFoundException {
        orderManagement.clicksOnOrderOnTheListWithStatus(number, status);
    }

    @And("^Add Rush order without FollowUpDate to VM Assistance SQ$")
    public void addRushOrderWithoutFollowUpDateToVMAssistanceSQ() {
        addOrderToVMAssistSQ("40020831");
    }

    @And("^Add Rush order with FollowUpDate to VM Assistance SQ$")
    public void addRushOrderWithFollowUpDateToVMAssistanceSQ() {
        addOrderToVMAssistSQ("40020822");
    }

    @And("^Add NoRush order without FollowUpDate to VM Assistance SQ$")
    public void addNoRushOrderWithoutFollowUpDateToVMAssistanceSQ() {
        addOrderToVMAssistSQ("80124474");
    }

    @And("^Add NoRush order with FollowUpDate to VM Assistance SQ$")
    public void addNoRushOrderWithFollowUpDateToVMAssistanceSQ() {
        addOrderToVMAssistSQ("80124470");
    }

    private void addOrderToVMAssistSQ(String orderNumber) {
        userPanel.userClicksOnOrderManagement();
        orderManagement.userTypesInOrderNumber(orderNumber);
        orderManagement.userDoubleClicksOnOrder();
        userPanel.waitUntilLoaderDisappears();
        Assert.assertTrue(orderManagement.userSeesOrderDetails());
        if(orderManagement.isVmAssistButtonDisplayed()) {
            orderManagement.userClicksOnVMAssistButton();
            Assert.assertTrue(orderManagement.userSeesConfirmVMAssistWindow());
            orderManagement.userTypesInInConfirmVMAssistWindow("test");
            orderManagement.userClicksOnConfirmOption();
            userPanel.waitUntilLoaderDisappears();
            Assert.assertEquals("Message is different", "VM Assist Task Created.", orderManagement.getPageMessage());
        }
    }

    @And("^Order is in Rush and without FollowUpDate$")
    public void orderIsInRushAndWithoutFollowUpDate() {
        Assert.assertTrue("Order is not in Rush!", orderManagement.isOrderInRush());
        Assert.assertTrue("Follow Up Date is not empty!", orderManagement.isFollowUpDateEmpty());
    }

    @And("^Order is not in Rush and without FollowUpDate$")
    public void orderIsNotInRushAndWithoutFollowUpDate() {
        Assert.assertFalse("Order is in Rush!", orderManagement.isOrderInRush());
        Assert.assertTrue("Follow Up Date is not empty!", orderManagement.isFollowUpDateEmpty());
    }

    @And("^Order is in Rush and with FollowUpDate$")
    public void orderIsInRushAndWithFollowUpDate() {
        Assert.assertTrue("Order is not in Rush!", orderManagement.isOrderInRush());
        Assert.assertFalse("Follow Up Date is empty!", orderManagement.isFollowUpDateEmpty());
    }

    @And("^Order is not in Rush and with FollowUpDate$")
    public void orderIsNotInRushAndWithFollowUpDate() {
        Assert.assertFalse("Order is in Rush!", orderManagement.isOrderInRush());
        Assert.assertFalse("Follow Up Date is empty!", orderManagement.isFollowUpDateEmpty());
    }

    @And("^Clear search bar$")
    public void clearSearchBar() {
        orderManagement.clearOrderNumberInput();
    }

    @And("^Add NoRush order with Note to VM Assistance SQ$")
    public void addNoRushOrderWithNoteToVMAssistanceSQ() {
        addOrderToVMAssistSQ("80110417");
    }

    @And("^Order is not in Rush and with Note$")
    public void orderIsNotInRushAndWithNote() {
        Assert.assertFalse("Order is in Rush!", orderManagement.isOrderInRush());
        Assert.assertTrue("Missing notes!", orderManagement.isAnyNoteAdded());
    }

    @And("^Add another NoRush order with FollowUpDate to VM Assistance SQ$")
    public void addAnotherNoRushOrderWithFollowUpDateToVMAssistanceSQ() {
        addOrderToVMAssistSQ("80124471");
    }

    @And("^Order has number \"([^\"]*)\"$")
    public void orderHasNumber(String number) {
        Assert.assertEquals("Order number is different", number, orderManagement.hasOrderNumber());
    }

    @And("^Message \"([^\"]*)\" is not displayed$")
    public void messageIsNotDisplayed(String message) {
        if(orderManagement.isPageMessageDisplayed())
            Assert.assertNotEquals("Message is different", message, orderManagement.getPageMessage());
    }

    @And("^User searches previous order number$")
    public void userSearchesPreviousOrderNumber() throws FileNotFoundException {
        orderManagement.openPreviousOrder();
    }
}
