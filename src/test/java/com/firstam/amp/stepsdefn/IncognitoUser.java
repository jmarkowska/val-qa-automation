package com.firstam.amp.stepsdefn;

import com.firstam.amp.utils.DriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;


public class IncognitoUser {

    private WebDriver driver = DriverFactory.getIncognitoDriver();
    private com.firstam.amp.pages.IncognitoUser incognitoUser = new com.firstam.amp.pages.IncognitoUser(driver);

    @And("^Incognito user clicks on logout$")
    public void userClicksOnLogout() {
        incognitoUser.logout();
    }
    
    @When("^Open incognito window$")
    public void openWindow() {
        incognitoUser.openIncognitoWindow();
    }

    @Then("^Close incognito window$")
    public void closeIncognitoWindow() {
        incognitoUser.closeIncognitoWindow();
    }

    @When("^User is logged in incognito with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void userIsLoggedInIncognitoWithUsernameAndPassword(String username, String password) {
        incognitoUser.logInToAmpIncognito(username, password);
    }

    @Given("^Incognito user clicks on Order Management$")
    public void incognitoUserClicksOnOrderManagement() {
        incognitoUser.userClicksOnOrderManagement();
    }

    @When("^Incognito user types in Order Number \"([^\"]*)\"$")
    public void incognitoUserTypesInOrderNumber(String orderNumber) {
        incognitoUser.userTypesInOrderNumber(orderNumber);
    }

    @And("^Incognito user double clicks on Order$")
    public void incognitoUserDoubleClicksOnOrder() {
        incognitoUser.userDoubleClicksOnOrder();
    }

    @Then("^Incognito user sees Order Details$")
    public void incognitoUserSeesOrderDetails() {
        Assert.assertTrue(incognitoUser.userSeesOrderDetails());
    }

    @And("^Incognito user clicks on Order$")
    public void incognitoUserClicksOnOrder() {
        incognitoUser.userClicksOnOrder();
    }

    @Then("^Incognito popup on locked order is displayed$")
    public void incognitoPopupOnLockedOrderIsDisplayed() {
        Assert.assertTrue(incognitoUser.isPopupOnLockedOrderDisplayed());
    }

    @When("^Incognito click Cancel on Unlock popup$")
    public void incognitoClickCancelOnUnlockPopup() {
        incognitoUser.clickCancelOnUnlockPopup();
    }

    @Then("^Incognito order number is red$")
    public void incognitoOrderNumberIsRed() {
        Assert.assertTrue(incognitoUser.isOrderNumberRed());
    }

    @And("^Incognito Order Locked By \"([^\"]*)\" in padlock message$")
    public void incognitoOrderLockedByInPadlockMessage(String username) {
        Assert.assertTrue(incognitoUser.isOrderLockedByInPadlockMessage(username));
    }

    @And("^Incognito hover over padlock$")
    public void incognitoHoverOverPadlock() {
        incognitoUser.hoverOverPadlock();
    }

    @Then("^Incognito Order is currently locked by \"([^\"]*)\" page message$")
    public void incognitoOrderIsCurrentlyLockedByPageMessage(String name)  {
        Assert.assertTrue("Order is locked by another user or it's not locked at all", incognitoUser.isOrderLockedByInPageMessage(name));
    }
}


