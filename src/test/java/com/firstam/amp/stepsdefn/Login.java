package com.firstam.amp.stepsdefn;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import com.firstam.amp.utils.DriverFactory;


public class Login {

    private WebDriver driver = DriverFactory.getDriver();
    private com.firstam.amp.pages.Login login = new com.firstam.amp.pages.Login(driver);


    @Given("^User is logged in with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void userIsLoggedInWithUsernameAndPassword(String username, String password) {
        login.enterUsername(username);
        login.enterPassword(password);
        login.clickSubmit();
    }

    @And("^User clicks on logout$")
    public void userClicksOnLogout() {
        login.logout();
    }
}


