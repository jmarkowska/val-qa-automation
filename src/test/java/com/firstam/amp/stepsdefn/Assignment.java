package com.firstam.amp.stepsdefn;


import com.firstam.amp.utils.DriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;


public class Assignment {

    private WebDriver driver = DriverFactory.getDriver();
    private com.firstam.amp.pages.Assignment assignment = new com.firstam.amp.pages.Assignment(driver);
    private com.firstam.amp.pages.OrderManagement orderManagement = new com.firstam.amp.pages.OrderManagement(driver);
    private com.firstam.amp.pages.UserPanel userPanel = new com.firstam.amp.pages.UserPanel(driver);

    @Then("^Assignment view is displayed$")
    public void assignmentViewIsDisplayed() {
        Assert.assertTrue("Assignment view is not displayed", assignment.isAssignmentViewDisplayed());
    }

    @When("^Select Vendor with username \"([^\"]*)\"$")
    public void selectVendorWithUsername(String vendor) {
        assignment.selectVendor(vendor);
    }

    @Then("^Confirm Assignment window is displayed$")
    public void confirmAssignmentWindowIsDisplayed() {
        Assert.assertTrue("Confirm Assignment window is not displayed", assignment.isConfirmAssignmentWindowDisplayed());
    }

    @When("^Enter \"([^\"]*)\" in Confirm Assignment text area if displayed$")
    public void enterInConfirmAssignmentTextArea(String text) {
        assignment.enterToConfirmAssignmentTextArea(text);
    }

    @And("^Assign button in Assignment Confirmation window is displayed$")
    public void assignButtonInAssignmentConfirmationWindowIsDisplayed() {
        Assert.assertTrue("Assign button is not displayed", assignment.isAssignButtonInAssignmentConfirmationWindowDisplayed());
    }

    @And("^User clicks on Assign button in Confirm Assignment window$")
    public void userClicksOnAssignButtonInConfirmAssignmentWindow() {
        assignment.clickOnAssignButtonInConfirmAssignmentWindow();
    }

    @Then("^Re-Assignment window is displayed$")
    public void reAssignmentWindowIsDisplayed() {
        Assert.assertTrue("Re-Assignment window is not displayed", assignment.isReAssignmentWindowDisplayed());
        Assert.assertTrue("Re-Assignment text area is not displayed", assignment.isReAssignmentTextAreaDisplayed());
    }

    @When("^Enter \"([^\"]*)\" in Re-Assignment text area$")
    public void enterInReAssignmentTextArea(String text) {
        assignment.enterToReAssignmentTextArea(text);
    }

    @And("^User selects \"([^\"]*)\" re-assignment reason$")
    public void userSelectReAssignmentReason(String reason) {
        assignment.selectReAssignmentReason(reason);
    }

    @And("^User clicks on Save button in Re-Assignment window$")
    public void userClicksOnSaveButtonInReAssignmentWindow() {
        assignment.saveReAssignment();
    }

    @And("^Close error window if displayed$")
    public void closeErrorWindowIfDisplayed() {
        assignment.closeErrorWindowIfDisplayed();
    }

    @And("^User clicks on Cancel button in Confirm Assignment window$")
    public void userClicksOnCancelButtonInConfirmAssignmentWindow() {
        assignment.clickCancelInConfirmAssignment();
    }

    @And("^Cancel button in Assignment Confirmation window is displayed$")
    public void cancelButtonInAssignmentConfirmationWindowIsDisplayed() {
        Assert.assertTrue("Cancel button is not displayed", assignment.isCancelButtonInAssignmentConfirmationWindowDisplayed());
    }

    @And("^User clicks on X button in Confirm Assignment window$")
    public void userClicksOnXButtonInConfirmAssignmentWindow() {
        assignment.clickXInConfirmAssignment();
    }

    @When("^Assign vendor if exists$")
    public void assignVendorIfExists() {
        if(!assignment.noDataInTable()) {
            assignment.selectFirstVendor();
            orderManagement.clickAssignButton();
            Assert.assertTrue("Confirm Assignment window is not displayed", assignment.isConfirmAssignmentWindowDisplayed());
            Assert.assertTrue("Assign button is not displayed", assignment.isAssignButtonInAssignmentConfirmationWindowDisplayed());
            assignment.enterToConfirmAssignmentTextArea("Test test test");
            assignment.clickOnAssignButtonInConfirmAssignmentWindow();
            if(assignment.isReAssignmentWindowDisplayed()) {
                assignment.selectReAssignmentReason("Other");
                assignment.enterToReAssignmentTextArea("test test");
                assignment.saveReAssignment();
            }
            userPanel.waitUntilLoaderDisappears();
            assignment.closeErrorWindowIfDisplayed();
//            Assert.assertEquals("Order status is wrong", "Pending", orderManagement.orderStatus());
        }
        else System.out.println("LOG No vendors available");
    }

    @When("^Select first vendor to assign$")
    public void selectFirstVendorToAssign() {
        assignment.selectFirstVendor();
    }

    @When("^Assign vendor if exists and close order view$")
    public void assignVendorIfExistsAndCloseOrderView() {
        if(!assignment.noDataInTable()) {
            assignment.selectFirstVendor();
            orderManagement.clickAssignAndCloseButton();
            Assert.assertTrue("Confirm Assignment window is not displayed", assignment.isConfirmAssignmentWindowDisplayed());
            Assert.assertTrue("Assign button is not displayed", assignment.isAssignButtonInAssignmentConfirmationWindowDisplayed());
            assignment.enterToConfirmAssignmentTextArea("Test test test");
            assignment.clickOnAssignButtonInConfirmAssignmentWindow();
            userPanel.waitUntilLoaderDisappears();
            assignment.closeErrorWindowIfDisplayed();
        }
        else System.out.println("LOG No vendors available");
    }
}
