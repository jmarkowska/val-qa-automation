package com.firstam.amp.stepsdefn;


import com.firstam.amp.utils.DriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;


public class UserPanel {

    private WebDriver driver = DriverFactory.getDriver();
    private com.firstam.amp.pages.UserPanel userPanel = new com.firstam.amp.pages.UserPanel(driver);

    @And("^User clicks on Order Management$")
    public void userClicksOnOrderManagement() {
        userPanel.userClicksOnOrderManagement();
    }

    @And("^Wait until loader disappears$")
    public void waitUntilLoaderDisappears() {
        userPanel.waitUntilLoaderDisappears();
    }

    @When("^User clicks on Impersonate$")
    public void userClicksOnImpersonate() {
        userPanel.clickImpersonate();
    }

    @Then("^Impersonate window is displayed$")
    public void impersonateWindowIsDisplayed() {
        Assert.assertTrue("Impersonate window is not displayed",userPanel.isImpersonateWindowDisplayed());
    }

    @When("^Enter \"([^\"]*)\" username, \"([^\"]*)\" password and \"([^\"]*)\" vendor$")
    public void enterUsernamePasswordAndImpersonate(String username, String password, String impersonate) {
        userPanel.enterCredentialsToImpersonateUser(username, password, impersonate);
    }

    @And("^Tap OK in Impersonate window$")
    public void tapOKInImpersonateWindow() {
        userPanel.clickOkInImpersonateWindow();
    }

    @Then("^User in not \"([^\"]*)\"$")
    public void userInNot(String user) {
        Assert.assertFalse("User is " + user, userPanel.userIsLoggedInAs(user));
    }

    @When("^User clicks on Work Queue tab$")
    public void userClicksOnWorkQueueTab() {
        userPanel.clickOnWorkQueueTab();
    }

    @Given("^User refreshes page$")
    public void userRefreshesPage() {
        driver.navigate().refresh();
    }

    @Given("^User clicks on Home tab$")
    public void userClicksOnHomeTab() {
        userPanel.clickHomeTab();
    }

    @Then("^Next button is displayed in Home tab$")
    public void nextButtonIsDisplayedInHomeTab() {
        Assert.assertTrue("Next button is not displayed", userPanel.isNextButtonDisplayed());
    }

    @When("^User clicks on Next button in Home tab$")
    public void userClicksOnNextButtonInHomeTab() {
        userPanel.clickNextButton();
    }

    @And("^Sleep$")
    public void sleep() throws InterruptedException {
        Thread.sleep(2000);
    }

    @And("^User waits (\\d+) minutes for lambda$")
    public void userWaitsMinutesForLambda(int interval) throws InterruptedException {
        Thread.sleep(interval*60*1000);
    }
}


