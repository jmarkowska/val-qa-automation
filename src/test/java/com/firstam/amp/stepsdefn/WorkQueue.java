package com.firstam.amp.stepsdefn;


import com.firstam.amp.utils.DriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.io.FileNotFoundException;


public class WorkQueue {

    private WebDriver driver = DriverFactory.getDriver();
    private com.firstam.amp.pages.WorkQueue workQueue = new com.firstam.amp.pages.WorkQueue(driver);

    @And("^Work Queue is displayed$")
    public void workQueueIsDisplayed() {
        Assert.assertTrue("Work Queue is not displayed", workQueue.isWorkQueueDisplayed());
    }

    @When("^Vendor types in Order Number \"([^\"]*)\"$")
    public void vendorTypesInOrderNumber(String order) {
        workQueue.enterOrderNumberToSearch(order);
    }

    @And("^Vendor opens Order Details$")
    public void vendorOpensOrderDetails() {
        workQueue.openOrderDetails();
    }

    @Then("^Vendor Order Details view is displayed$")
    public void vendorOrderDetailsViewIsDisplayed() {
        Assert.assertTrue("Order Details view is not displayed", workQueue.isVendorOrderDetailsViewDisplayed());
    }

    @And("^Vendor Reject button is displayed$")
    public void vendorRejectButtonIsDisplayed() {
        Assert.assertTrue("Vendor Reject button is not displayed", workQueue.isVendorRejectButtonDisplayed());
    }

    @And("^Vendor Accept button is displayed$")
    public void vendorAcceptButtonIsDisplayed() {
        Assert.assertTrue("Vendor Accept button is not displayed", workQueue.isVendorAcceptButtonDisplayed());
    }

    @When("^Vendor clicks Reject button$")
    public void vendorClicksRejectButton() {
        workQueue.clickRejectButton();
    }

    @Then("^Deny Order window is displayed$")
    public void denyOrderWindowIsDisplayed() {
        Assert.assertTrue("Deny Order window is displayed", workQueue.isDenyOrderWindowDisplayed());
    }

    @When("^Vendor select reject reason$")
    public void vendorSelectRejectReason() {
        workQueue.selectRejectReason();
    }

    @When("^Vendor enters \"([^\"]*)\" reason comment$")
    public void vendorEntersReasonComment(String comment) {
        workQueue.enterRejectReasonComment(comment);
    }

    @And("^Vendor clicks on Deny Order$")
    public void vendorClicksOnDenyOrder() {
        workQueue.clickDenyOrder();
    }

    @Then("^Empty order list is displayed$")
    public void emptyOrderListIsDisplayed() {
        Assert.assertTrue("Empty order list is not displayed", workQueue.isEmptyOrderListDisplayed());
    }

    @When("^Vendor searches previous order number$")
    public void vendorSearchesPreviousOrderNumber() throws FileNotFoundException {
        workQueue.enterPreviousOrderNumberToSearch();
    }
}


