package com.firstam.amp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class Assignment extends AbstractPage {

    public Assignment(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//span[text()='Order Assignment']")
    private WebElement assignmentView;
    @FindBy(xpath = "//div[@role=\"dialog\"]/div/span[text()='Confirm Assignment']")
    private WebElement confirmAssignmentWindow;
    @FindBy(xpath = "//div[@role=\"dialog\"]/div/span[text()='Re-Assignment Reasons']")
    private WebElement reAssignmentWindow;
    @FindBy(css = "textarea#txtLessScorer")
    private WebElement textAreaConfirmAssignment;
    @FindBy(css = "textarea#txtLessScorer")
    private List<WebElement> textAreas;
    @FindBy(id = "txtReassignReason")
    private WebElement textAreaReAssignment;
    @FindBy(xpath = "//button[@class='ui-button ui-corner-all ui-widget'][text()='Assign']")
    private WebElement assignButtonInConfirmAssignment;
    @FindBy(xpath = "//button[@class='ui-button ui-corner-all ui-widget'][text()='Cancel']")
    private WebElement cancelButtonInConfirmAssignment;
    @FindBy(xpath = "//button[@class='ui-button ui-corner-all ui-widget'][text()='Save']")
    private WebElement saveReAssignment;
    @FindBy(xpath = "//button[@title='Close']")
    private List<WebElement> closeWindowButton;
    @FindBy(xpath = "//div[@role=\"dialog\"]/div/span[text()='Error']")
    private List<WebElement> errorWindow;
    @FindBy(xpath = "//td[text()='No data available in table']")
    private List<WebElement> noDataText;
    @FindBy(xpath = "(//tbody/tr[contains(@class, 'data-row-whiteBk')])[1]")
    private WebElement firstVendor;


    public boolean isAssignmentViewDisplayed() {
        return assignmentView.isDisplayed();
    }

    public void selectVendor(String username) {
        WebElement vendor = driver.findElement(By.xpath("//tr[@id='" + username + "']"));
        vendor.click();
    }

    public boolean isConfirmAssignmentWindowDisplayed() {
        return confirmAssignmentWindow.isDisplayed();
    }

    public boolean isReAssignmentWindowDisplayed() {
        try {
            return reAssignmentWindow.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean isReAssignmentTextAreaDisplayed() {
        try {
            return textAreaReAssignment.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void enterToConfirmAssignmentTextArea(String text) {
        if(!textAreas.isEmpty()) textAreaConfirmAssignment.sendKeys(text);
    }

    public void enterToReAssignmentTextArea(String text) {
        textAreaReAssignment.sendKeys(text);
    }

    public boolean isAssignButtonInAssignmentConfirmationWindowDisplayed() {
        try {
            return assignButtonInConfirmAssignment.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void clickOnAssignButtonInConfirmAssignmentWindow() {
        assignButtonInConfirmAssignment.click();
    }

    public void selectReAssignmentReason(String reason) {
        WebElement checkbox = driver.findElement(By.xpath("//label[text()='" + reason + "']/.."));
        checkbox.click();
    }

    public void saveReAssignment() {
        saveReAssignment.click();
    }

    public void closeErrorWindowIfDisplayed() {
        if(!errorWindow.isEmpty()) {
            closeWindowButton.get(1).click();
            System.out.println("ERR: INTERNAL SERVER ERROR");
        }
    }

    public void clickCancelInConfirmAssignment() {
        cancelButtonInConfirmAssignment.click();
    }

    public boolean isCancelButtonInAssignmentConfirmationWindowDisplayed() {
        try {
            return cancelButtonInConfirmAssignment.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void clickXInConfirmAssignment() {
        closeWindowButton.get(1).click();
    }

    public void selectFirstVendor() {
        firstVendor.click();
    }

    public boolean noDataInTable() {
        return !noDataText.isEmpty();
    }
}


