package com.firstam.amp.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import com.firstam.amp.utils.Helpers;


public class Login extends AbstractPage {

    public Login(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_cpCorp_loginctrl_UserName")
    private WebElement usernameField;
    @FindBy (id = "ctl00_cpCorp_loginctrl_Password")
    private WebElement passwordField;
    @FindBy (id = "ctl00_cpCorp_loginctrl_Login")
    private WebElement loginButton;
    @FindBy (linkText = "Logout")
    private WebElement logoutButton;


    public void enterUsername(String username){
        Helpers.waitForElement(usernameField);
        usernameField.sendKeys(username);
    }

    public void enterPassword (String password){
        Helpers.waitForElement(passwordField);
        passwordField.sendKeys(password);
    }

    public void clickSubmit(){
        loginButton.click();
    }

    public void logout () {
        Helpers.waitForElement(logoutButton);
        logoutButton.click();
    }
}
