package com.firstam.amp.pages;

import com.firstam.amp.utils.Helpers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class WorkQueue extends AbstractPage {

    public WorkQueue(WebDriver driver) {
        super(driver);
    }
    private String openedOrderID;

    @FindBy(xpath = "//span[@id='ctl00_cpAppContent_lblTitleLeft'][text()='Work Queue']")
    private WebElement workQueueTitle;
    @FindBy(id = "ctl00_cpAppContent_tcWorkQueue_tpPending_txtOrderNo")
    private WebElement orderIdSearch;
    @FindBy(id = "ctl00_cpAppContent_tcWorkQueue_tpPending_agWorkQueue_ctl02_btnConfirmOrder")
    private WebElement orderDetailsButton;
    @FindBy(id = "ctl00_cpAppContent_lblTitleLeft")
    private WebElement orderDetailsViewTitle;
    @FindBy(id = "ctl00_cpAppContent_btnDeny")
    private WebElement rejectButton;
    @FindBy(id = "ctl00_cpAppContent_btnAccept")
    private WebElement acceptButton;
    @FindBy(id = "ctl00_DENY_lblTitle")
    private WebElement denyOrderWindow;
    @FindBy(xpath = "//input[@id='ctl00_DENY_dlReasons_ctl10_rbReason']")
    private WebElement cannotCompleteByDueDateReason;
    @FindBy(id = "ctl00_DENY_txtReason")
    private WebElement reasonCommentInput;
    @FindBy(id = "ctl00_DENY_btnDeny")
    private WebElement denyOrderButton;
    @FindBy(xpath = "//td[text()='No orders match search criteria.']")
    private WebElement emptyOrderList;

    public boolean isWorkQueueDisplayed() {
        try {
            Helpers.waitForElement(workQueueTitle);
            return workQueueTitle.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void enterOrderNumberToSearch(String order) {
        orderIdSearch.sendKeys(order);
        orderIdSearch.submit();
    }

    public void openOrderDetails() {
        orderDetailsButton.click();
    }

    public boolean isVendorOrderDetailsViewDisplayed() {
        try {
            return orderDetailsViewTitle.getText().contains(openedOrderID);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isVendorRejectButtonDisplayed() {
        try {
            return rejectButton.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean isVendorAcceptButtonDisplayed() {
        try {
            return acceptButton.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void clickRejectButton() {
        rejectButton.click();
    }

    public boolean isDenyOrderWindowDisplayed() {
        try {
            return denyOrderWindow.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void selectRejectReason() {
        Helpers.jsClickRadioButton(cannotCompleteByDueDateReason);
    }

    public void enterRejectReasonComment(String comment) {
        reasonCommentInput.sendKeys(comment);
    }

    public void clickDenyOrder() {
        Helpers.jsClickOnElement(denyOrderButton);
    }

    public boolean isEmptyOrderListDisplayed() {
        try {
            return emptyOrderList.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void enterPreviousOrderNumberToSearch() throws FileNotFoundException {
        if(openedOrderID == null) {
            File file = new File("orderID.txt");
            Scanner sc = new Scanner(file);
            openedOrderID = sc.nextLine();
        }
        System.out.println(openedOrderID);
        orderIdSearch.sendKeys(openedOrderID);
        orderIdSearch.submit();
    }
}
