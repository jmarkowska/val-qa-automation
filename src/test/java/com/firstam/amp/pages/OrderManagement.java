package com.firstam.amp.pages;

import com.firstam.amp.utils.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;


public class OrderManagement extends AbstractPage {

    public OrderManagement(WebDriver driver) {
        super(driver);
    }
    private com.firstam.amp.pages.UserPanel userPanel = new com.firstam.amp.pages.UserPanel(driver);
    private String firstOrderID;
    private String secondOrderID;
    private String currentOrderID;
    private String openedOrderID;

    @FindBy(id = "ctl00_cpAppContent_txtOrderNo")
    private WebElement orderNumberInput;
    @FindBy(xpath = "//span[@id='ctl00_smpBreadcrumb']/span[text()='Order Details']")
    private WebElement orderDetailsView;
    @FindBy(id = "ctl00_cpAppContent_btnWFVMA")
    private WebElement vMAssistButton;
    @FindBy(xpath = "//div[@role=\"dialog\"]/div/span[text()='Confirm VM Assist']")
    private WebElement confirmVmAssistWindow;
    @FindBy(xpath = "//div[@class='ui-dialog-buttonset']/button[text()='Cancel']")
    private WebElement cancelButtonInConfirmVMAssistWindow;
    @FindBy(xpath = "//div[@class='ui-dialog-buttonset']/button[text()='Confirm']")
    private WebElement confirmButtonInConfirmVMAssistWindow;
    @FindBy(css = "textarea#txtVMAssistNote")
    private WebElement textAreaVMAssistWindow;
    @FindBy(css = "#ctl00_cpAppContent_btnSave.fg-button.ui-state-default.ui-priority-primary.ui-corner-left")
    private WebElement saveButton;
    @FindBy(css = "input#ctl00_cpAppContent_btnWFComplete.fg-button.ui-state-default.ui-priority-primary")
    private WebElement completeTaskButton;
    @FindBy(css = "#ctl00_cpAppContent_btnBack.fg-button.ui-state-default.ui-priority-primary.ui-corner-all")
    private WebElement backButton;
    @FindBy(css = " #ctl00_progressLoad")
    private WebElement progressLoad;
    @FindBy(id = "ctl00_cpAppContent_btnWFNext")
    private WebElement nextButton;
    @FindBy(id = "ctl00_cpAppContent_InfoBox_MessageDisplay")
    private WebElement pageMessage;
    @FindBy(id = "ctl00_cpAppContent_InfoBox_MessageDisplay")
    private List<WebElement> pageMessages;
    @FindBy(id = "ctl00_cpAppContent_btnWFVMA")
    private WebElement vmAssistButton;
    @FindBy(id = "ctl00_cpAppContent_lblOrderNo")
    private WebElement orderDetailsNumber;
    @FindBy(id = "ctl00_cpAppContent_btnAssign")
    private WebElement assignButton;
    @FindBy(id = "ctl00_cpAppContent_btnAssignQ")
    private WebElement assignAndCloseButton;
    @FindBy(id = "ctl00_cpAppContent_lblProductStatus")
    private WebElement orderStatus;
    @FindBy(id = "ctl00_cpAppContent_agOMQueue_ctl13_GridViewPager1_ImageButtonNext")
    private WebElement nextOrderListPageButton;
    @FindBy(xpath = "//tr[@class='data-row']")
    private WebElement orderList;
    @FindBy(id = "ctl00_cpAppContent_agOMQueue_ctl02_lblVendor")
    private WebElement orderVendor;
    @FindBy(id = "ctl00_cpAppContent_btnFollowUpDate")
    private WebElement followUpDateCalendarIcon;
    @FindBy(id = "ctl00_FUDID_lbltitle")
    private WebElement followUpDateWindow;
    @FindBy(id = "ctl00_FUDID_txtNewETA")
    private WebElement followUpDateInput;
    @FindBy(id = "ctl00_FUDID_txtFollowupTime")
    private WebElement followUpTimeInput;
    @FindBy(id = "ctl00_FUDID_btnSave")
    private WebElement saveFollowUpDateButton;
    @FindBy(id = "ctl00_cpAppContent_lblRush")
    private List<WebElement> orderRush;
    @FindBy(id = "ctl00_cpAppContent_lblFollowUpDate")
    private WebElement followUpDate;
    @FindBy(id = "ctl00_cpAppContent_lblOrderNo")
    private WebElement orderNumber;
    @FindBy(xpath = "//div[@class='redFlag16']")
    private List<WebElement> noteRedFlags;


    public void userTypesInOrderNumber(String orderNumber) {
        orderNumberInput.clear();
        orderNumberInput.sendKeys(orderNumber);
        orderNumberInput.submit();
    }

    public void userDoubleClicksOnOrder() {
        WebElement order = driver.findElement(By.xpath("//span[@id='ctl00_cpAppContent_agOMQueue_ctl02_lblOrderNo']"));
        actions.doubleClick(order).perform();
    }

    public boolean userSeesOrderDetails() {
        return orderDetailsView.isDisplayed();
    }

    public void userClicksOnVMAssistButton() {
        try {
            vMAssistButton.click();
        } catch(Exception e) {
            completeTaskButton.click();
            userPanel.waitUntilLoaderDisappears();
            vMAssistButton.click();
        }
    }

    public boolean userSeesConfirmVMAssistWindow() {
        try {
            return confirmVmAssistWindow.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean userSeesCancelButtonInConfirmVMAssistWindow() {
        try {
            return cancelButtonInConfirmVMAssistWindow.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean userSeesConfirmButtonInConfirmVMAssistWindow() {
        try {
            return confirmButtonInConfirmVMAssistWindow.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void userClicksOnCancelOption() {
        cancelButtonInConfirmVMAssistWindow.click();
    }

    public void userClicksOnConfirmOption() {
        confirmButtonInConfirmVMAssistWindow.click();
    }

    public void userTypesInInConfirmVMAssistWindow(String text) {
        textAreaVMAssistWindow.sendKeys(text);
    }

    public void userClicksSaveButton() {
        Helpers.waitForElementToBeGone(progressLoad, 15);
        saveButton.click();
    }

    public void userClicksOnCompleteTaskButton() {
        Helpers.waitForElementToBeGone(progressLoad, 15);
        completeTaskButton.click();
    }

    public boolean userSeesVMAssistButton() {
        Helpers.waitForElementToBeGone(progressLoad, 15);
        return vMAssistButton.isDisplayed();
    }

    public void userClicksOnBackButton() {
        Helpers.waitForElementToBeGone(progressLoad, 15);
        backButton.click();
    }

    public void clicksOnOrderOnTheList(int number) {
        WebElement order = driver.findElement(By.xpath("(//tr[contains(@class,'data-row')]/td[5]/span)[" + number + "]"));
        actions.doubleClick(order).perform();
    }

    public void clickNextButton() {
        Helpers.waitForElement(nextButton);
        nextButton.click();
    }

    public boolean isCompleteTaskDisplayed() {
        try {
            return completeTaskButton.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean isVmAssistButtonDisplayed() {
        try {
            return vmAssistButton.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean isPageMessageDisplayed() {
        return !pageMessages.isEmpty();
    }

    public boolean isVmAssistTaskCompletedMessageDisplayed() {
        Helpers.waitForElement(pageMessage);
        return pageMessage.getText().contains("VM Assist Task Completed");
    }

    public void setGlobalOrderID() {
        try {
            if (firstOrderID == null) firstOrderID = orderDetailsNumber.getText();
            else if (secondOrderID == null) secondOrderID = orderDetailsNumber.getText();
            currentOrderID = orderDetailsNumber.getText();
            System.out.println(firstOrderID);
            System.out.println(secondOrderID);
            System.out.println(currentOrderID);
        } catch(Exception e) {
            System.out.println("LOG No order opened. Cannot set order ID.");
        }
    }

    public boolean isSecondOrderIdTheSameAsTheFirstOne() {
        System.out.println("LOG first order ID: " + firstOrderID);
        System.out.println("LOG second order ID: " + secondOrderID);
        return firstOrderID.equals(secondOrderID);
    }

    public boolean isThirdOrderIdTheSameAsTheFirstOne() {
        System.out.println("LOG first order ID: " + firstOrderID);
        System.out.println("LOG third order ID: " + currentOrderID);
        return firstOrderID.equals(currentOrderID);
    }

    public boolean isThirdOrderIdTheSameAsTheSecondOne() {
        System.out.println("LOG second order ID: " + secondOrderID);
        System.out.println("LOG third order ID: " + currentOrderID);
        return secondOrderID.equals(currentOrderID);
    }

    public void clickAssignButton() {
        Helpers.waitForElement(assignButton);
        assignButton.click();
    }

    public String orderStatus() {
        Helpers.waitForElement(orderStatus);
        return orderStatus.getText();
    }

    public String getPageMessage() {
        return pageMessage.getText();
    }

    public void goToNextPage() {
        nextOrderListPageButton.click();
    }

    public void clickAssignAndCloseButton() {
        assignAndCloseButton.click();
    }

    public boolean isOrderListDisplayed() {
        try {
            return orderList.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public String getVendorAssignedToOrder() {
        return orderVendor.getText();
    }

    public void setFollowUpDateInTheFuture() {
        String[] prevDate = followUpDateInput.getAttribute("value").split("/");
        String newDate;
        if(prevDate[0].contains("_")) {
            newDate = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + "/" +Calendar.getInstance().get(Calendar.MONTH) + "/" + (Calendar.getInstance().get(Calendar.YEAR)+1);
        }
        else newDate = prevDate[0]+"/"+prevDate[1]+"/"+(Integer.parseInt(prevDate[2])+1);
        followUpDateInput.clear();
        followUpDateInput.sendKeys(newDate);
        followUpDateInput.sendKeys(Keys.chord(Keys.ENTER));
    }

    public void openFollowUpDateCalendar() {
        followUpDateCalendarIcon.click();
    }

    public boolean isFollowUpDateWindowDisplayed() {
        try {
            return followUpDateWindow.isDisplayed() && followUpDateWindow.getText().contains("Add Follow Up date");
        } catch(Exception e) {
            return false;
        }
    }

    public void saveNewFollowUpDate() {
        saveFollowUpDateButton.click();
    }

    public void clicksOnOrderOnTheListWithStatus(int number, String status) throws FileNotFoundException {
        WebElement order = driver.findElement(By.xpath("(//tr[contains(@class,'data-row')]/td[7]/span[text()='" + status + "'])[" + number + "]"));
        openedOrderID = driver.findElement(By.xpath("(//tr[contains(@class,'data-row')]/td[7]/span[text()='" + status + "'])[" + number + "]/../../td[5]/span")).getText();
        PrintWriter writer = new PrintWriter("orderID.txt");
        writer.println(openedOrderID);
        writer.close();
        System.out.println(openedOrderID);
        actions.doubleClick(order).perform();
    }

    public boolean isOrderInRush() {
        return !orderRush.isEmpty();
    }

    public boolean isFollowUpDateEmpty() {
        return followUpDate.getText().length() == 0;
    }

    public void clearOrderNumberInput() {
        orderNumberInput.clear();
        orderNumberInput.submit();
        userPanel.waitUntilLoaderDisappears();
    }

    public boolean isAnyNoteAdded() {
        return !noteRedFlags.isEmpty();
    }

    public String hasOrderNumber() {
        try {
            return orderNumber.getText();
        } catch(Exception e) {
            return "";
        }
    }

    public boolean isPageMessageEmpty() {
        try {
            return pageMessage.getText().isEmpty();
        } catch(Exception e) {
            return true;
        }
    }

    public void openPreviousOrder() throws FileNotFoundException {
        orderNumberInput.clear();
        if(openedOrderID == null) {
            File file = new File("orderID.txt");
            Scanner sc = new Scanner(file);
            openedOrderID = sc.nextLine();
        }
        System.out.println(openedOrderID);
        orderNumberInput.sendKeys(openedOrderID);
        orderNumberInput.submit();
    }
}


