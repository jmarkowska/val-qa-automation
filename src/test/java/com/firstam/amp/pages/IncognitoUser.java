package com.firstam.amp.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import com.firstam.amp.utils.Helpers;
import org.openqa.selenium.support.PageFactory;


public class IncognitoUser extends AbstractPage {
    private final WebDriver driver;
    private Actions actions;

    public IncognitoUser(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
    }

    @FindBy(id = "ctl00_cpCorp_loginctrl_UserName")
    private WebElement usernameField;
    @FindBy (id = "ctl00_cpCorp_loginctrl_Password")
    private WebElement passwordField;
    @FindBy (id = "ctl00_cpCorp_loginctrl_Login")
    private WebElement loginButton;
    @FindBy (linkText = "Logout")
    private WebElement logoutButton;
    @FindBy(id = "ctl00_cpAppContent_txtOrderNo")
    private WebElement orderNumberInput;
    @FindBy(xpath = "//span[@id='ctl00_smpBreadcrumb']/span[text()='Order Details']")
    private WebElement orderDetailsView;
    @FindBy(id = "ctl00_cpAppContent_btnWFVMA")
    private WebElement vMAssistButton;
    @FindBy(xpath = "//div[@role=\"dialog\"]/div/span[text()='Confirm VM Assist']")
    private WebElement confirmVmAssistWindow;
    @FindBy(xpath = "//div[@class='ui-dialog-buttonset']/button[text()='Cancel']")
    private WebElement cancelButtonInConfirmVMAssistWindow;
    @FindBy(xpath = "//div[@class='ui-dialog-buttonset']/button[text()='Confirm']")
    private WebElement confirmButtonInConfirmVMAssistWindow;
    @FindBy(css = "textarea#txtVMAssistNote")
    private WebElement textAreaVMAssistWindow;
    @FindBy(css = "#ctl00_cpAppContent_btnSave.fg-button.ui-state-default.ui-priority-primary.ui-corner-left")
    private WebElement saveButton;
    @FindBy(css = "input#ctl00_cpAppContent_btnWFComplete.fg-button.ui-state-default.ui-priority-primary")
    private WebElement completeTaskButton;
    @FindBy(css = "#ctl00_cpAppContent_btnBack.fg-button.ui-state-default.ui-priority-primary.ui-corner-all")
    private WebElement backButton;
    @FindBy(css = " #ctl00_progressLoad")
    private WebElement progressLoad;
    @FindBy(id = "ctl00_OMLink")
    private WebElement orderManagment;
    @FindBy(xpath = "//span[@id='ctl00_cpAppContent_agOMQueue_ctl02_lblOrderNo']")
    private WebElement foundOrder;
    @FindBy(xpath = "//div[contains(@class, 'ui-dialog-titlebar')]/span[@class='ui-dialog-title'][text()='Unlock']")
    private WebElement popupOnLockedOrder;
    @FindBy(xpath = "//div[@class='ui-dialog-buttonset']/button[text()='Cancel']")
    private WebElement cancelOnUnlockPopup;
    @FindBy(id = "ctl00_cpAppContent_agOMQueue_ctl02_lblStatus")
    private WebElement foundOrderRow;
    @FindBy(css = ".padlock28.ttTriggerLock")
    private WebElement padlock;
    @FindBy(xpath = "//div[@class='ui-tooltip-content']/span[@id='ctl00_cpAppContent_ucLockedTT_lblLockedBy']")
    private WebElement lockedBy;
    @FindBy(id = "ctl00_cpAppContent_InfoBox_MessageDisplay")
    private WebElement pageMessage;


    public void logout () {
        Helpers.waitForElement(logoutButton);
        logoutButton.click();
    }

    public void openIncognitoWindow() {
        driver.get("https://devamp.firstam.com/Resources/Home.aspx?ReturnUrl=%2F");
    }

    public void logInToAmpIncognito(String username, String password) {
        Helpers.waitForElement(usernameField);
        usernameField.sendKeys(username);
        passwordField.sendKeys(password);
        loginButton.click();
    }

    public void closeIncognitoWindow() {
        driver.close();
    }

    public void userTypesInOrderNumber(String orderNumber) {
        orderNumberInput.sendKeys(orderNumber);
        orderNumberInput.submit();
    }

    public void userClicksOnOrderManagement() {
        Helpers.waitForElement(orderManagment);
        orderManagment.click();
    }

    public void userDoubleClicksOnOrder() {
        Helpers.waitForElement(foundOrderRow);
        actions.doubleClick(foundOrderRow).perform();
    }

    public void userClicksOnOrder() {
        foundOrder.click();
    }

    public boolean userSeesOrderDetails() {
        try {
            Helpers.waitForElementToBeGone(progressLoad, 15);
            return orderDetailsView.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public boolean isPopupOnLockedOrderDisplayed() {
        try {
            return popupOnLockedOrder.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void clickCancelOnUnlockPopup() {
        cancelOnUnlockPopup.click();
    }

    public boolean isOrderNumberRed() {
        return foundOrder.getAttribute("class").contains("ttTriggerLock red");
    }

    public boolean isOrderLockedByInPadlockMessage(String username) {
        return lockedBy.getText().contains(username);
    }

    public void hoverOverPadlock() {
        actions.moveToElement(padlock).perform();
    }

    public boolean isOrderLockedByInPageMessage(String name) {
        return pageMessage.getText().contains(name);
    }
}
