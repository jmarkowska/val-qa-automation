package com.firstam.amp.pages;

import com.firstam.amp.utils.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.interactions.Actions;


public class AbstractPage {

    protected final WebDriver driver;
    final Actions actions;

    AbstractPage() {
        driver = DriverFactory.getDriver();
        actions = new Actions(driver);
    }

    AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.actions = new Actions(driver);
        PageFactory.initElements(driver, this);
    }
}