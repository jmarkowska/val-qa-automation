package com.firstam.amp.pages;

import com.firstam.amp.utils.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UserPanel extends AbstractPage {

    public UserPanel(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ctl00_OMLink")
    private WebElement orderManagment;
    @FindBy(css = " #ctl00_progressLoad")
    private WebElement progressLoad;
    @FindBy(xpath = "//div[@class='msg'][text()='Impersonate']")
    private WebElement impersonateWindow;
    @FindBy(id = "ctl00_lnkImpersonate")
    private WebElement impersonateLink;
    @FindBy(id = "ctl00_IPS_txtUsername")
    private WebElement usernameImpersonate;
    @FindBy(id = "ctl00_IPS_txtPassword")
    private WebElement passwordImpersonate;
    @FindBy(id = "ctl00_IPS_txtImpUsername")
    private WebElement vendorImpersonate;
    @FindBy(id = "ctl00_IPS_btnSave")
    private WebElement okButtonImpersonate;
    @FindBy(id = "ctl00_LoginName")
    private WebElement loggedInUser;
    @FindBy(id = "ctl00_WQLink")
    private WebElement workQueueTab;
    @FindBy(xpath = "//a[text()='Home']")
    private WebElement homeTab;
    @FindBy(id = "ctl00_cpAppContent_btnWFNext")
    private WebElement nextButton;


    public void userClicksOnOrderManagement() {
        try {
            Helpers.waitForElement(orderManagment);
            orderManagment.click();
        } catch (Exception e) {
            System.out.println("LOG element not clickable");
        }
    }

    public void waitUntilLoaderDisappears() {
        if (!driver.findElements(By.cssSelector(" #ctl00_progressLoad")).isEmpty()) {
            new WebDriverWait(driver, 45).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(" #ctl00_progressLoad")));
        }
    }

    public void clickImpersonate() {
        Helpers.waitForElement(impersonateLink);
        impersonateLink.click();
    }

    public boolean isImpersonateWindowDisplayed() {
        return impersonateWindow.isDisplayed();
    }

    public void enterCredentialsToImpersonateUser(String username, String password, String impersonate) {
        usernameImpersonate.sendKeys(username);
        passwordImpersonate.sendKeys(password);
        vendorImpersonate.sendKeys(impersonate);
    }

    public void clickOkInImpersonateWindow() {
        okButtonImpersonate.click();
    }

    public boolean userIsLoggedInAs(String user) {
        return loggedInUser.getText().equals(user);
    }

    public void clickOnWorkQueueTab() {
        Helpers.waitForElement(workQueueTab);
        workQueueTab.click();
    }

    public void clickHomeTab() {
        homeTab.click();
    }

    public boolean isNextButtonDisplayed() {
        try {
            Helpers.waitForElement(nextButton);
            return nextButton.isDisplayed();
        } catch(Exception e) {
            return false;
        }
    }

    public void clickNextButton() {
        nextButton.click();
    }
}
