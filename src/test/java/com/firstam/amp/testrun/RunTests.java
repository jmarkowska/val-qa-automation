package com.firstam.amp.testrun;

import com.firstam.amp.utils.DriverFactory;
import com.firstam.amp.utils.RestTest;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/",
        glue = {"com.firstam.amp.stepsdefn"},
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
        strict = true,
        monochrome = true,
        tags = {"@grape_smoke"}
)
public class RunTests {
    @AfterClass
    public static void tearDown() throws Exception {
        DriverFactory.getDriver().quit();
        RestTest.addTextExecution();
    }
}
