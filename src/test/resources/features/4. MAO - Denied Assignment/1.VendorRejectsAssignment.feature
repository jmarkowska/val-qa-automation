@grape_smoke
@grape_regression

@VALSQ-179

Feature: MAO - Vendor rejects order assignment
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-179

  Scenario: Impersonate Vendor
    Given User is logged in with username "a13488" and password "Hello123!"
    When User clicks on Impersonate
    Then Impersonate window is displayed
    When Enter "a13488" username, "Hello123!" password and "a13159" vendor
    And Tap OK in Impersonate window
    And Wait until loader disappears
    Then User in not "Jane Doe"

  Scenario: Reject Order
    When User clicks on Work Queue tab
    Then Work Queue is displayed
    When Vendor searches previous order number
    And Vendor opens Order Details
    And Wait until loader disappears
    Then Vendor Order Details view is displayed
    And Vendor Accept button is displayed
    And Vendor Reject button is displayed
    When Vendor clicks Reject button
    Then Deny Order window is displayed
    When Vendor select reject reason
    And Vendor enters "Cannot complete by due date" reason comment
    And Vendor clicks on Deny Order
    And Wait until loader disappears
    Then Empty order list is displayed

  Scenario: 0- Logout
    Given User clicks on logout