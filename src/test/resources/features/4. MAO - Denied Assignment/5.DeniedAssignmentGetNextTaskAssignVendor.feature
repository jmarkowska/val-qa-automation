@grape_smoke
@grape_regression

@VALSQ-313

Feature: MAO - get Next Denied Assignment Task assign vendor
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-313

  Scenario: 0- Login
    Given User is logged in with username "a13488" and password "Hello123!"

  Scenario: Get next Denied Assignment Task
    Given User clicks on Order Management
    When User clicks on 1 order with "Unassigned" status on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then User sees Order Details
    And Message "Manually Assign Order - Denied Assignment" is displayed

  Scenario: Assign vendor
    Given Order status is "Denied"
    When User clicks on Assign button
    And Wait until loader disappears
    Then Assignment view is displayed
    When Select first vendor to assign
    And User clicks on Assign button
    Then Confirm Assignment window is displayed
    And Assign button in Assignment Confirmation window is displayed
    When Enter "test" in Confirm Assignment text area if displayed
    And User clicks on Assign button in Confirm Assignment window
    Then Re-Assignment window is displayed
    When Enter "test test" in Re-Assignment text area
    And User selects "Other" re-assignment reason
    And User clicks on Save button in Re-Assignment window
    And Wait until loader disappears
    And Close error window if displayed
#    Then Order status is "Pending"

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
