@grape_smoke
@grape_regression

@VALSQ-207

Feature: MAO - get Next Denied Assignment Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-207

  Scenario: 0- Login
    Given User is logged in with username "a13488" and password "Hello123!"

  Scenario: Get next Denied Assignment Task
    Given User clicks on Order Management
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then User sees Order Details
    And Message "Manually Assign Order - Denied Assignment" is displayed

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
