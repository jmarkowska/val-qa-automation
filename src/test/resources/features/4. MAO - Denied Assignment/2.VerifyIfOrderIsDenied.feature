@grape_smoke
@grape_regression

@VALSQ-221

Feature: MAO - Denied Assignment - verify order status
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-221

  Scenario: 0- Login
    Given User is logged in with username "a13488" and password "Hello123!"

  Scenario: Verify if Order is Denied
    Given User clicks on Order Management
    When User searches previous order number
    And User double clicks on Order
    And Wait until loader disappears
    Then User sees Order Details
    And Order status is "Denied"

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
