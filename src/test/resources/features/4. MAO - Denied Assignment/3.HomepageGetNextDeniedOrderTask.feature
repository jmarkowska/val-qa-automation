@grape_smoke
@grape_regression

@VALSQ-317

Feature: Denied - Assignment - Homepage get Next Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-317

  Scenario: 0- Login
    Given User is logged in with username "a13488" and password "Hello123!"

  Scenario: Get Next Denied Order Task
    Given User clicks on Home tab
    And User waits 2 minutes for lambda
    Then Next button is displayed in Home tab
    When User clicks on Next button in Home tab
    And Wait until loader disappears
    And Message "Manually Assign Order – Denied Assignment" is displayed
    And Order status is "Denied"

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout