@grape_smoke
@grape_regression

@VALSQ-108

Feature: VM assistance request Cancel
  AC: https://firstamerican.atlassian.net/browse/VALSQ-29
  TC: https://firstamerican.atlassian.net/browse/VALSQ-108

  Scenario: 0- Login
    Given User is logged in with username "a13484" and password "Hello123!"

  Scenario: VM assistance request - User select Cancel Option
    Given User clicks on Order Management
    When User clicks on 2 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on VM Assist button
    Then User sees Confirm VM Assist window
    And User sees Cancel button in confirm VM Assist window
    When User clicks on Cancel option
    Then User sees Order Details




