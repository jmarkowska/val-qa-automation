@grape_smoke0
@grape_regression

@VALSQ-288

Feature: VM Assistance Task priority when lender date is the same
  AC: https://firstamerican.atlassian.net/browse/VALSQ-25
  TC: https://firstamerican.atlassian.net/browse/VALSQ-288

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"
    When User clicks on Order Management
    And User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And User completes all VM Assist Tasks
    And Add Rush order without FollowUpDate to VM Assistance SQ
    And Add Rush order with FollowUpDate to VM Assistance SQ
    And Add NoRush order without FollowUpDate to VM Assistance SQ
    And Add NoRush order with Note to VM Assistance SQ
    And Add NoRush order with FollowUpDate to VM Assistance SQ

  Scenario: 0- Re-log
    Given User clicks on Order Management
    And User clicks on logout
    Then User is logged in with username "a13491" and password "Hello123!"

  Scenario: VM assistance - User gets Next task
    Given User clicks on Order Management
    And Clear search bar
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    And User waits 2 minutes for lambda
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Order is in Rush and without FollowUpDate

  Scenario: VM assistance - User completes task
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: VM assistance - User gets Next task
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Order is not in Rush and with Note

  Scenario: VM assistance - User completes task
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: VM assistance - User gets Next task
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Order is not in Rush and without FollowUpDate

  Scenario: VM assistance - User completes task
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: VM assistance - User gets Next task
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Order is in Rush and with FollowUpDate

  Scenario: VM assistance - User completes task
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: VM assistance - User gets Next task
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Order is not in Rush and with FollowUpDate

  Scenario: VM assistance - User completes task
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout