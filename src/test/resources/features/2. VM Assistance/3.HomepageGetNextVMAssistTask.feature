@grape_smoke
@grape_regression

@VALSQ-315

Feature: VM Assistance - Assignment - Homepage get Next Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-30
  TC: https://firstamerican.atlassian.net/browse/VALSQ-315

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"

  Scenario: Get Next VM Assistance Task
    Given User clicks on Home tab
    And User waits 2 minutes for lambda
    Then Next button is displayed in Home tab
    When User clicks on Next button in Home tab
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And VM Assist button is not displayed
    And Complete Task button is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout