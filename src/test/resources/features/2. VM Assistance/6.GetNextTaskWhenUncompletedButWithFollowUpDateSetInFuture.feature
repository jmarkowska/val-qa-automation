@grape_smoke
@grape_regression

@VALSQ-241

Feature: VM Assistance - Assignment - get Next Task [previous uncompleted but with follow up date set in the future]
  AC: https://firstamerican.atlassian.net/browse/VALSQ-30
  TC: https://firstamerican.atlassian.net/browse/VALSQ-241

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"
    And Add 3 orders to VM Assistance SQ

  Scenario: VM assistance - User gets three Next tasks
    Given User clicks on Order Management
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    And User waits 2 minutes for lambda
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Complete Task button is displayed

    When User sets Follow Up date in the future
    And User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Second order ID is different from the first one
    And Complete Task button is displayed

    When User sets Follow Up date in the future
    And User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And Third order ID is different from previous
    And Complete Task button is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout