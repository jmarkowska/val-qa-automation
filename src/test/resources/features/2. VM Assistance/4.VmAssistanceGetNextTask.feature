@grape_smoke
@grape_regression

@VALSQ-181

Feature: VM Assistance - Assignment - get Next Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-30
  TC: https://firstamerican.atlassian.net/browse/VALSQ-181

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"

  Scenario: Get Next VM Assistance Task
    Given User clicks on Order Management
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then Message "Vendor Management Assistance" is displayed
    And VM Assist button is not displayed
    And Complete Task button is displayed
    When User clicks on Complete Task button
    And Wait until loader disappears
    Then Message "VM Assist Task Completed." is displayed
    And VM Assist button is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout