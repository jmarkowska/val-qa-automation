@grape_smoke
@grape_regression

@VALSQ-220

Feature: VM Assistance - Assignment - Complete all Tasks from SQ
  AC: https://firstamerican.atlassian.net/browse/VALSQ-30
  TC: https://firstamerican.atlassian.net/browse/VALSQ-220

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"

  Scenario: Get next VM Assistance Task
    Given User clicks on Order Management
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And User completes all VM Assist Tasks
    Then Message "End of Queue." is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout