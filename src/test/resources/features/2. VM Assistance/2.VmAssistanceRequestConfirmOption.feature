@grape_smoke
@grape_regression

@VALSQ-116

Feature: VM assistance request Confirm
  AC: https://firstamerican.atlassian.net/browse/VALSQ-29
  TC: https://firstamerican.atlassian.net/browse/VALSQ-116

  Scenario: VM assistance request - User select Confirm Option
    Given User clicks on VM Assist button
    When User sees Confirm VM Assist window
    And User sees Confirm button in confirm VM Assist window
    And User types in "Test" in Confirm VM Assist window
    When User clicks on Confirm option
    And Wait until loader disappears
    Then Message "VM Assist Task Created." is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout

