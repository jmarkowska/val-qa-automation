@grape_smoke
@grape_regression

@VALSQ-280

Feature: MAO - get Next Unaccepted Order Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-123
  TC: https://firstamerican.atlassian.net/browse/VALSQ-280

  Scenario: 0- Login
    Given User is logged in with username "a13489" and password "Hello123!"

  Scenario: Get next Unaccepted Task
    Given User clicks on Order Management
    When User clicks on 1 order on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then User sees Order Details
    And Message "Manually Assign Order -  Unaccepted/Timeout" is displayed

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout