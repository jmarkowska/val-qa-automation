@grape_smoke
@grape_regression

@VALSQ-316

Feature: Unaccepted Order - Homepage get Next Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-123
  TC: https://firstamerican.atlassian.net/browse/VALSQ-316

  Scenario: 0- Login
    Given User is logged in with username "a13489" and password "Hello123!"

  Scenario: Get Next Unaccepted Order Task
    Given User clicks on Home tab
    Then Next button is displayed in Home tab
    When User clicks on Next button in Home tab
    And Wait until loader disappears
    And Message "Manually Assign Order -  Unaccepted/Timeout" is displayed

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout