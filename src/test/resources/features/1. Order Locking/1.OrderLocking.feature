@grape_smoke
@grape_regression

@VALSQ-194

Feature: Order Locking - 2 users in parallel
  AC: https://firstamerican.atlassian.net/browse/VALSQ-73
  TC: https://firstamerican.atlassian.net/browse/VALSQ-194

  Scenario: C0- Login
    Given User is logged in with username "a13484" and password "Hello123!"

  Scenario: Open order and lock it
    Given User clicks on Order Management
    When User types in Order Number "90086541"
    And User double clicks on Order
    And Wait until loader disappears
    Then User sees Order Details

  Scenario: Simulate second user in incognito
    Given Open incognito window
    When User is logged in incognito with username "a13488" and password "Hello123!"
    Given Incognito user clicks on Order Management
    When Incognito user types in Order Number "90086541"
    Then Incognito order number is red
    And Incognito user clicks on Order
    Then Incognito popup on locked order is displayed
    When Incognito click Cancel on Unlock popup
    And Incognito user double clicks on Order
    Then Incognito user sees Order Details
    Then Incognito Order is currently locked by "Grape Up" page message
    And Incognito hover over padlock
    And Incognito Order Locked By "a13484" in padlock message

  Scenario: C0- Close incognito window
    Then Close incognito window

  Scenario: C0- Logout
      Then User clicks on logout