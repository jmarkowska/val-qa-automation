@grape_smoke
@grape_regression

@VALSQ-318

Feature: Unassigned Order - Homepage get Next Task
  AC: https://firstamerican.atlassian.net/browse/VALSQ-119
  TC: https://firstamerican.atlassian.net/browse/VALSQ-318

  Scenario: 0- Login
    Given User is logged in with username "a13490" and password "Hello123!"

  Scenario: Get Next Unassigned Order Task
    Given User clicks on Home tab
    Then Next button is displayed in Home tab
    When User clicks on Next button in Home tab
    And Wait until loader disappears
    And Message "Manually Assign Order – Unassigned" is displayed
    And Order status is "Unassigned"

  Scenario: 0- Logout
    Given User clicks on Order Management
    And User clicks on logout