@grape_smoke
@grape_regression

@VALSQ-228

Feature: MAO - get Next Unassigned Order Task, assign vendor and close
  AC: https://firstamerican.atlassian.net/browse/VALSQ-119
  TC: https://firstamerican.atlassian.net/browse/VALSQ-228

  Scenario: 0- Login
    Given User is logged in with username "a13490" and password "Hello123!"

  Scenario: Get next Unassigned Task
    Given User clicks on Order Management
    When User clicks on 1 order with "Pending" status on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then User sees Order Details
    And Message "Manually Assign Order - Unassigned" is displayed
    And Order status is "Unassigned"

  Scenario: Assign vendor if exists
    When User clicks on Assign button
    And Wait until loader disappears
    Then Assignment view is displayed
    When Assign vendor if exists and close order view

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
