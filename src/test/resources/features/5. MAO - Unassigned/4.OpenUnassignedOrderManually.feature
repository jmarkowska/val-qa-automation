@grape_smoke
@grape_regression

@VALSQ-226

Feature: MAO - Task not returned when Order opened manually
  AC: https://firstamerican.atlassian.net/browse/VALSQ-119
  TC: https://firstamerican.atlassian.net/browse/VALSQ-226

  Scenario: 0- Login
    Given User is logged in with username "a13490" and password "Hello123!"

  Scenario: Open Unassigned Task
    Given User clicks on Order Management
    When User clicks on 1 order with "Unassigned" status on the list
    And Wait until loader disappears
    Then User sees Order Details
    And Message "Manually Assign Order - Unassigned" is not displayed

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
