@grape_smoke
@grape_regression

@VALSQ-225

Feature: MAO - get Next Task when user can't work on Unassigned queue
  AC: https://firstamerican.atlassian.net/browse/VALSQ-119
  TC: https://firstamerican.atlassian.net/browse/VALSQ-225

  Scenario: 0- Login
    Given User is logged in with username "a13491" and password "Hello123!"

  Scenario: Get next Unassigned Task
    Given User clicks on Order Management
    When User clicks on 1 order with "Pending" status on the list
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Next button
    And Wait until loader disappears
    Then User sees Order Details
    And Order status is not "Unassigned"

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on Order Management
    And User clicks on logout
