@grape_smoke
@grape_regression

@VALSQ-205

Feature: MAO - Assign Vendor to order and Close
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-205

  Scenario: 0- Login
    Given User is logged in with username "a13484" and password "Hello123!"

  Scenario: Assign Vendor to order
    Given User clicks on Order Management
    When User clicks on 1 order with "Unassigned" status on the list
    And User double clicks on Order
    And Wait until loader disappears
    Then User sees Order Details
    When User clicks on Assign button
    And Wait until loader disappears
    Then Assignment view is displayed
    When Select Vendor with username "jsallay"
    And User clicks on Assign and Close button
    Then Confirm Assignment window is displayed
    And Assign button in Assignment Confirmation window is displayed
    When Enter "test" in Confirm Assignment text area if displayed
    And User clicks on Assign button in Confirm Assignment window
    And Wait until loader disappears
    And Close error window if displayed
#    Then Order list is displayed

  Scenario: Verify assignment
    When User clicks on Order Management
    When User searches previous order number
    Then Vendor "jsallay" is assigned to the order

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on logout
