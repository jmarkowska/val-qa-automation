@grape_smoke
@grape_regression

@VALSQ-202

Feature: MAO - Assign Vendor to order - Cancel
  AC: https://firstamerican.atlassian.net/browse/VALSQ-94
  TC: https://firstamerican.atlassian.net/browse/VALSQ-202

  Scenario: 0- Login
    Given User is logged in with username "a13484" and password "Hello123!"

  Scenario: Assign Vendor to order
    Given User clicks on Order Management
    When User clicks on 1 order with "Unassigned" status on the list
    And Wait until loader disappears
    When User clicks on Assign button
    And Wait until loader disappears
    Then Assignment view is displayed
    When Select Vendor with username "jsallay"
    And User clicks on Assign button
    Then Confirm Assignment window is displayed
    And Cancel button in Assignment Confirmation window is displayed
    When Enter "test" in Confirm Assignment text area if displayed
    And User clicks on Cancel button in Confirm Assignment window
    Then Assignment view is displayed

  Scenario: Verify assignment
    When User clicks on Order Management
    And User searches previous order number
    Then Vendor "jsallay" is not assigned to the order

  Scenario: 0- Logout
    Given User refreshes page
    And User clicks on logout
